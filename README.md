# BY-COVID BioImage Archive MINERVA Plugin

This is the repository for a MINERVA plugin handling the BioImage Archive dataset [S-BIAD29](https://www.ebi.ac.uk/biostudies/bioimages/studies/S-BIAD29).

The plugin is tailored  to be used with the COVID-19 Disease Map to visualise drug screening data available at the BioImage Archive. This dataset contains the effects of antiviral drugs and their effects on the SARS-CoV-2 infected cell lines.

**To launch the plugin:**
- go to the COVID-19 Disease Map hosted on the MINERVA Platform (https://covid19map.elixir-luxembourg.org/minerva/)
- load the plugin using the menu icon (three horizontal lines, upper left corner)
- Find the "by-covid-bioimage-archive" plugin and press "Load" button (see below)

![load](./img/plugin_load.png)

**To use the plugin:**
- browse the list of compounds (rightmost panel)
- targets of the selected compounds are dynamically loaded in the map area (middle panel)

![load](./img/plugin_example.png)

Many thanks to Aastha Mathur, Isabel Kemmer and Phil Gribbon from tbe BY-COVID project [by-covid.eu](https://by-covid.eu) for their help with developing the plugin. 

The plugin was developed with the support of the EU Horizon funding, [grant agreement ID: 101046203](https://cordis.europa.eu/project/id/101046203)