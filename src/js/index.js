const minervaWrapper = require('minerva-wrapper');
const MinervaElement = minervaWrapper.minerva.Element

require('../css/styles.css');
require('../css/minerva.css');
require('../assets/material-icons.css')
require('../assets/bootstrap-table-1.12/bootstrap-table.min.css');

minervaWrapper.setGlobalJQuery();

try {

// bootstrap-table-1.12 is required directly because when using via npm, the boostrapTable is for unknown reason not added to the $ object when deployed on minerva-dev
    require('../assets/bootstrap-table-1.12/bootstrap-table.min');

    const pluginName = 'by-covid-bioimage-archive';
    const pluginVersion = '1.1.0';

    const containerName = pluginName + '-container';

    const settings = {
        dataSource: 'https://minerva-service.lcsb.uni.lu/plugins/bycovid-bioimage/bioimage_archive_data.json',
        imagesBaseLink: "https://www.ebi.ac.uk/biostudies/files/S-BIAD29/",
        pubchemBaseLink: 'https://pubchem.ncbi.nlm.nih.gov/compound/',
        chemblBaseLink: 'https://www.ebi.ac.uk/chembl/compound_report_card/',
        maxHighlighted: 50,
    };

    const globals = {
        adrData: [],
        target: {},
        projectId: "",
        selected: [],
    };

    let $ = window.$;

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

    let pluginContainer;

    const register = function (_minerva) {
        $(".tab-content").css('position', 'relative');

        globals.projectId = minervaWrapper.getProjectId();

        pluginContainer = $(minervaWrapper.getElement());

        return initPlugin();
    };

    const unregister = function () {
        unregisterListeners();
        return deHighlightAll();
    };

    // noinspection JSIgnoredPromiseFromCall
    minervaWrapper.init({
        register: register,
        unregister: unregister,
        name: pluginName,
        version: pluginVersion,
        minWidth: 400,
        defaultWidth: 500,
        pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/bycovid-bioimage/plugin.js',
    });


    function initPlugin() {
        registerListeners();
        // Showing a loading div
        const container = initMainPageStructure();

        // Loading data, then hiding the loading div and showing the table
        loadRawData().then(() => {
            container.find('.compounds-loading').hide();
            initTableStructure(container);
            initDataTable(container);
        })
    }

    function registerListeners() {
    }

    function unregisterListeners() {
        minervaWrapper.removeAllListeners();
    }

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


    function deHighlightAll() {
        const table = $('.bioimages-table');
        table.find('.row-select-checkbox').each((index, checkbox) => {
            checkbox.checked = false
        });
        return minervaWrapper.getHighlightedBioEntities().then(highlighted => {
            return minervaWrapper.hideBioEntity(highlighted.map(function (entry) {
                return {
                    element: {
                        id: entry.element.getId(),
                        modelId: entry.element.getModelId(),
                        type: "ALIAS"
                    },
                    type: "ICON",
                    bioEntity: entry.element,
                }
            }));
        });

    }

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************


    function getContainerClass() {
        return containerName;
    }

    /**
     * Creates the table and its toolbar
     */
    function initTableStructure(container) {
        container.append(`
        <div id="toolbar">
            <button id="filter-selected-btn" class="btn btn-secondary">Filter selected</button>
            <button id="unfilter-btn" class="btn btn-secondary">Show all</button>
        </div>
        <table class="bioimages-table table" data-toggle="table"></table>
    `)
    }

    /**
     * Creates the main container and display a loading page
     */
    function initMainPageStructure() {
        const container = $(`
        <div class="${getContainerClass()}">
            
        </div>
    `).appendTo(pluginContainer);

        container.append(`
        <div class="panel card panel-default compounds-loading">
            <div class="panel-body card-body">  
                <i class="fa fa-circle-o-notch fa-spin"></i> Loading data mapping. This might take several minutes
                if this is the first time the plugin is loaded for this map ...
            </div>        
        </div>
    `)
        return container;
    }

    /**
     * Load the data to be displayed
     */
    async function loadRawData() {
        globals.targets = await retrieveElements(); // Retrieve available targets from minerva API

        let elements = [];
        for (let name in globals.targets) {
            if (Object.prototype.hasOwnProperty.call(globals.targets, name)) {
                globals.targets[name].forEach(element => {
                    elements.push(element.minervaElement);
                });
            }
        }

        globals.parentConnections = await retrieveConnections(elements);

        return fetch(settings.dataSource, {method: "GET"})
            .then((response) => response.json())
            .then((json) => {
                globals.adrData = json.map((item, index) => {
                    let targets = [];
                    if (item.list_map_targets) {
                        if (typeof item.list_map_targets === 'string' || item.list_map_targets instanceof String) {
                            targets = [item.list_map_targets];
                        } else {
                            targets = item.list_map_targets;
                        }
                    }
                    return {
                        ...item.compound,                               // The data displayed in the main table
                        "id": index,                                    // The unique id to use when updating data
                        "state": false,                                 // The checkboxes check state
                        "targets": targets,                             // The list of targets for highlighting
                        "images": item.images || []                     // The data for sub-tables in detail-view
                    }
                });
            })
            .catch((error) => {
                console.error("An error occurred while fetching data:", error);
            });
    }

    async function retrieveConnections(elements) {
        const baseUrl = window.location.origin;
        let connections = await $.ajax({
            type: 'GET',
            url: `${baseUrl}/minerva/api/projects/${globals.projectId}/submapConnections/`,
            dataType: 'json'
        });

        let parentElementsByModelId = [];

        connections.forEach(function (connection) {
            let targetModelId = connection.to.modelId;
            if (parentElementsByModelId[targetModelId] === undefined) {
                parentElementsByModelId[targetModelId] = getParentElementsForModelId(targetModelId, connections, elements);
            }
        });

        return parentElementsByModelId;
    }

    /**
     *
     * @param {number} targetModelId
     * @param {array} connections
     * @param {array<MinervaElement>} elements
     */
    function getParentElementsForModelId(targetModelId, connections, elements) {
        let result = [];
        let usedModelIds = [];
        usedModelIds[targetModelId] = true;

        let toCheck = [targetModelId];
        while (toCheck.length > 0) {
            let currentTargetId = toCheck.shift();
            connections.forEach(function (connection) {
                if (connection.to.modelId === currentTargetId && usedModelIds[connection.from.modelId] === undefined) {
                    result.push(getElementById(connection.from.id, elements));
                    toCheck.push(connection.to.modelId);
                    usedModelIds[connection.from.modelId] = true;
                }
            });
        }
        return result;
    }

    function getElementById(id, elements) {
        let results = elements.filter(function (element) {
            return element.id === id;
        });
        return results[0];
    }


    /**
     * Build Url to download images based on the link in data.
     * Example:
     * `20200802-ftp/SARS-CoV2[2454]/GUF0000021[6128]/2020-03-13T201014+0100[6440]/2020-03-13T201014+0100[6440]/008010-1-001001001.tif`
     * will be transformed into
     * `20200802-ftp/SARS-CoV2%5B2454%5D/GUF0000021%5B6128%5D/2020-03-13T201014%2B0100%5B6440%5D/2020-03-13T201014%2B0100%5B6440%5D/008010-1-001001001.tif`
     */
    function buildImgLink(link) {
        return link.split("/").map((component) => encodeURIComponent(component)).join("/")
    }


    /**
     * Formats the byte size of files into a human-readable format
     * Example: `2334208` becomes `2.2MB`
     */
    function formatByteSize(bytes) {
        if (bytes === 0 || bytes === "0")
            return "0B";

        const k = 1024;
        const decimals = 1;
        const sizes = ["B", "KB", "MB", "GB", "TB"];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(decimals)) + sizes[i];
    }


    /**
     * @typedef {Object} ImageRaw
     * @property {string} Plate
     * @property {string} Well
     * @property {string} path
     * @property {number} size
     */

    /**
     * Returns the html to build a sub-table in detail-view.
     * @param {ImageRaw[]} data
     */
    function showSubRow(data) {
        const subTable = $(`
        <table class="table table-bordered table-hover w-100">
            <thead>
                <tr>
                    <th>Plate</th>
                    <th>Value</th>
                    <th>Size</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody class="table-body"></tbody>
        </table>
    `)

        const tableBody = subTable.find('tbody');
        for (const row of data) {
            tableBody.append(`
            <tr>
                <td>${row.Plate}</td>
                <td>${row.Well}</td>
                <td>${formatByteSize(row.size)}</td>
                <td><a href=${settings.imagesBaseLink + buildImgLink(row.path)}  target="_blank" class="imageDownloadLink material-icons">download</a></td>
            </tr>
        `)
        }
        return subTable;
    }

    /**
     * Method called when a checkbox is selected
     * This gets the list of targets associated with the checked row and call Minerva to highlight them
     */
    async function selectRowForHighlight() {
        const table = $('.bioimages-table');

        table.find('.bs-checkbox').each((index, item) => {
            item.disabled = true
        });

        try {
            const checkboxes = $('.bioimages-table input[type="checkbox"]');

            let highlightDefs = [];
            for (let i = 0; i < checkboxes.length; i++) {
                const checkbox = checkboxes[i];
                const jqCheckBox = $(checkbox).context;
                const rowIndex = jqCheckBox.parentElement.parentElement.attributes['data-index'].value;
                const rowId = jqCheckBox.parentElement.parentElement.attributes['data-uniqueid'].value;

                // 'getData' only returns the data correctly displayed
                const rowData = table.bootstrapTable('getData')[rowIndex];

                if (jqCheckBox.checked) {
                    // Updating global data (to maintain table state)
                    globals.adrData[rowId].state = true;

                    // Updating bootstrap table internal state
                    table.bootstrapTable('updateByUniqueId', {
                        id: rowId,
                        row: {
                            state: true
                        }
                    });

                    rowData.targets
                        .filter((item) => globals.targets[item.toLowerCase()] !== undefined)
                        .forEach((item) => {
                            globals.targets[item.toLowerCase()].forEach(element => {
                                highlightDefs.push(getMarkerForBioEntity(element.minervaElement));
                                if (globals.parentConnections[element.minervaElement.getModelId()] !== undefined) {
                                    globals.parentConnections[element.minervaElement.getModelId()].forEach(parentBioEntity => {
                                        highlightDefs.push(getMarkerForBioEntity(parentBioEntity));
                                    });
                                }
                            })
                        });
                } else {
                    // Updating global data (to maintain table state)
                    globals.adrData[rowId].state = false;

                    // Updating bootstrap table internal state
                    table.bootstrapTable('updateByUniqueId', {
                        id: rowId,
                        row: {
                            state: false
                        }
                    });
                }
            }

            let used = [];

            let markers = [];

            for (let i = 0; i < highlightDefs.length; i++) {
                if (!used[highlightDefs[i].element.id]) {
                    used[highlightDefs[i].element.id] = true;
                    markers.push(highlightDefs[i]);
                }
            }

            await deHighlightAll();
            await minervaWrapper.showBioEntity(markers);

        } finally {
            table.find('.bs-checkbox').each((index, item) => {
                item.disabled = false
            });
        }
    }

    /**
     * Building the bootstrap table initial state
     */
    function initDataTable(container) {
        const dataTable = container.find('.bioimages-table');
        const table = dataTable.bootstrapTable({
            data: globals.adrData,
            uniqueId: "id",
            columns: [{
                field: "state",
                title: "",
                searchable: false,
                sortable: false,
                formatter: (value) => `<input class="bs-checkbox" type="checkbox" ${value && "checked"}>`,
            }, {
                field: "id",
                title: "ID",
                visible: false, // We do not want this column to appear, it is only used internally
                searchable: false,
            }, {
                field: "pubchem_cid",
                title: 'PUBCHEM ID',
                data: "pubchem_cid",
                searchable: true,
            }, {
                field: "CompoundName",
                title: 'Name',
                data: "CompoundName",
                searchable: true
            }, {
                field: "unichem_link",
                title: 'Link',
                data: "unichem_link",
                clickToSelect: false,
                searchable: false, // Searching on this makes no sense
                formatter: (value) => `<a href=${encodeURI(value) || "#"}  target="_blank">${value ? value.split('/').at(-1).slice(0, 8) + '...' : "-"}</a>`
            }],
            search: true,
            responsive: true,
            sortName: 'id',
            sortOrder: 'asc',
            detailView: true,
            detailViewAlign: "right",   // Is ignored with bootstrap-table < 1.15.2
            detailViewIcon: false,   // Is ignored with bootstrap-table < 1.15.2
            detailViewByClick: true,
            maintainMetaData: true, // Is ignored with bootstrap-table < 1.15.2
            clickToSelect: true,
            detailFormatter: (index, row) => showSubRow(row.images),
        })

        // When row is clicked, display or hide the details associated with that row
        table.on("click-cell.bs.table.bs.table", function (e, field, value, row, jqElem) {
            e.preventDefault();
            if (field === "state") {
                return;
            }
            const shouldCollapse = jqElem.parent().next('tr').hasClass("detail-view");
            table.bootstrapTable('collapseAllRows');
            const index = jqElem.parent().attr('data-index');
            shouldCollapse ?
                table.bootstrapTable('collapseRow', index) :
                table.bootstrapTable('expandRow', index);
        });

        // Adding events for toolbar buttons and for checking the boxes
        $('#filter-selected-btn').on('click', () => {
            dataTable.bootstrapTable('filterBy', {state: true})
        });
        $('#unfilter-btn').on('click', () => {
            dataTable.bootstrapTable('filterBy', {})
        });
        table.on("change", ".bs-checkbox", selectRowForHighlight);
    }

    /**
     * Query the Minerva API to retrieve the list of available highlighting targets
     */
    function retrieveElements() {
        const baseUrl = window.location.origin;
        return $.ajax({
            type: 'GET',
            url: `${baseUrl}/minerva/api/projects/${globals.projectId}/models/*/bioEntities/elements/`,
            dataType: 'json'
        }).then((elements) => {
            const results = {};
            elements.forEach(function (element) {
                if (results[element.name.toLowerCase()] === undefined) {
                    results[element.name.toLowerCase()] = [];
                }
                results[element.name.toLowerCase()].push({
                    id: element.id,
                    modelId: element.modelId,
                    type: "ALIAS",
                    minervaElement: new MinervaElement(element)
                });
            });
            return results;
        });
    }


    function getMarkerForBioEntity(bioEntity) {
        return {
            element: {
                id: bioEntity.getId(),
                modelId: bioEntity.getModelId(),
                type: 'ALIAS'
            },
            type: "ICON",
            options: {
                color: '#FF0000',
                opacity: 1
            },
            bioEntity: bioEntity
        };
    }


} catch (e) {
    console.error(e);
    throw e;
}